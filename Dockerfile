FROM openresty/openresty:alpine-fat

# default config for fallback selfsigned certificate
ARG SSL_C=FR
ARG SSL_ST=Paris
ARG SSL_L=Paris
ARG SSL_O=Self hosted
ARG SSL_OU=Localhost
ARG SSL_CN=localhost

RUN apk add --no-cache --virtual .build-deps \
	openssl \
        gcc \
        libc-dev \
        make \
        openssl-dev \
        pcre-dev \
        zlib-dev \
        linux-headers \
        gnupg \
        libxslt-dev \
        gd-dev \
        geoip-dev \
        perl-dev \
        tar \
        unzip \
        zip \
        unzip \
        g++ \
        cmake \
        lua \
        lua-dev \
        make \
        autoconf \
        automake

RUN luarocks install lua-resty-auto-ssl

# Generate self-signed certificate for fallback
RUN mkdir  /etc/nginx/certs/
RUN openssl req \
	-x509 -nodes -days 3650 -newkey rsa:2048 \
	-keyout /etc/nginx/certs/default.key \
	-out /etc/nginx/certs/default.crt \
	-subj "/C=${SSL_C}/ST=${SSL_ST}/L=${SSL_L}/O=${SSL_O}/OU=${SSL_OU}/CN=${SSL_DOMAIN}"

# Create folder for lua-resty-auto-ssl
RUN mkdir -p /etc/resty-auto-ssl/storage/file
RUN chown -R nobody:nobody /etc/resty-auto-ssl/storage/file


COPY --chown=root:root ./conf.d/nginx.conf /usr/local/openresty/nginx/conf/nginx.conf
COPY --chown=root:root ./conf.d/default.conf /etc/nginx/conf.d/default.conf

RUN nginx -t
