# openresty-auto-ssl

## build
```
docker build -f Dockerfile -t openresty-auto-ssl .
```

## run

```sh
mkdir certs
chown -R nobody:nogroup certs/


docker run \
  -p 0.0.0.0:80:80 \
  -p 0.0.0.0:443:443 \
  -e SSL_DOMAIN_REGEX='(example.com)$' \
  -v $(pwd)/certs:/etc/resty-auto-ssl/storage/file \
  openresty-auto-ssl
```

